import type {PageServerLoad } from "./$types";
import { homePage } from "$lib/fun/phrases";
import api from "$lib/api"
import { singleItemCache } from "$lib/api/cache"
import { memberCache } from "$lib/api/cache";
import type { Member } from "$lib/api/types";

export const load: PageServerLoad = async () => {
    let phrase = homePage[~~(Math.random() * homePage.length)]

    let item: Member = {
        name: "Uh.",
        avatar_url: "",
        color: "ff0000"
    }

    try {
        item = await singleItemCache(phrase.id, memberCache, () => api<Member>(`${phrase.type}/${phrase.id}`))
    } catch (e: unknown) {
        item = {...item, ...{
            name: (e as Error).message
        }}
    }


    return {
        phrase: {
            name: item.name,
            pfp: item.webhook_avatar_url ?? item.avatar_url,
            text: phrase.text,
            color: item.color
        }
    }
}