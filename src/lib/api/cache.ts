import type { CacheEntry, Member } from "./types"

export let memberCache: CacheEntry<Member>[] = []

export async function singleItemCache<T>(id: string, cache: CacheEntry<T>[], callback: () => Promise<T>): Promise<T> {
    let maxAge: number = 20 * 60 * 1000
    let now = Date.now()
    for (let [index, entry] of cache.entries()) {
        if ((entry as any).item.id !== id) continue
        if (now > entry.expires.getTime()) {
            // console.log(`Cache entry (${id}) has expired by ${(now - entry.expires.getTime()) / 1000} seconds`)
            
            const item: T = await callback()
            
            let expires = new Date()
            expires.setTime(expires.getTime() + maxAge)
            const newEntry = {
                item: item,
                expires: expires
            }
            cache[index] = newEntry
            // console.log(`Updated cache entry! (${id})`)
            
            return item
        } else {
            // console.log(`Cache entry is still fresh and tasty (${id}). It expires in ${(entry.expires.getTime() - new Date().getTime()) / 1000} seconds`)
            return entry.item
        }
    }
    console.log(`No cache entry found for (${id}). Creating one now!`)
    const item: T = await callback()
    
    let expires = new Date()
    expires.setTime(expires.getTime() + maxAge)
    
    const newEntry = {
        item: item,
        expires: expires
    }

    cache.push(newEntry)
    return item
}
