interface APIOptions {
    token?: string,
    headers?: Record<string,string>
}

export default async function<T>(path: string, method: string = "GET", body: any = null, options?: APIOptions): Promise<T> {
    const resp = await fetch(`https://api.pluralkit.me/v2/${path}`, {
        method: method,
        headers: {
            ...(options && options.token ? { Authorization: options.token} : {}),
            ...(options && options.headers ? options.headers : {}),
            "Content-Type": "application/json",
        },
        body: body ? JSON.stringify(body) : null
    })

    if (resp.status === 500) throw new Error("500: PK Error")
    if (resp.status === 404) throw new Error("404: Not Found")
    if (resp.status === 403) throw new Error("403: Forbidden")
    if (resp.status === 401) throw new Error("401: Unauthorized")
    if (resp.status === 429) throw new Error("409: Rate Limited")

    const data = await resp.json()
    return data;
}