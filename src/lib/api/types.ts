export interface Member {
    name: string,
    display_name?: string,
    avatar_url?: string,
    webhook_avatar_url?: string,
    color?: string,
}

export interface CacheEntry<T> {
    expires: Date
    item: T
}