type Phrase = {
    type: "systems" | "groups" | "members",
    id: string,
    text: string
}

export const homePage: Phrase[] = [
    {
        type: "members",
        id: "fulmn",
        text: "Sometimes we'll give some info and tidbits in a dialogue format like this! Also hi, I'm the host lmao."
    },
    {
        type: "members",
        id: "radon",
        text: "Hiiii, co-host here. Yeah we'll interject from time to time. Get used to it!"
    },
    {
        type: "members",
        id: "mtndw",
        text: "Sup. I'm not either of the hosts but I figured I'd introduce you to this either way. Sometimes we'll come around and say something in these lil blurbs. It's kinda funny [if you have the same humor as us]."
    },
    {
        type: "members",
        id: "jevil",
        text: "We'll come in and say random nonsense sometimes! Always contained in these boxes, though I can't promise we won't escape from time to time."
    },
    {
        type: "members",
        id: "hngry",
        text: "Hi hi, we'll interject whenever our contribution is least appreciated."
    }
]